<?php
    /*
     Plugin Name: Homepage Slider
     Plugin URI: http://www.insp.com/
     Description: Custom Management panel for Wordpress Content Sliders.
     Version: 1.4
     Author: Clark Tomlinson
     Author URI: http://www.clarkt.com/
     License: This plugin is licensed under the GNU General Public License.
    */

    /**
     * Homepage Slider Handles all Slider Management Logic
     */
    class HomepageSlider
        {
        /**
         * Being Adding Hooks and Filters
         */
            public function __construct()
                {
                    $this->addHooks();
                    $this->addFilters();
                }

            public function addHooks()
                {
                    register_activation_hook(__FILE__, array($this, 'install'));
                    register_uninstall_hook(__FILE__, HomepageSlider::uninstall());

                    if (isset($_GET['page']) && $_GET['page'] == 'homepage-slider/homeslider.php') {
                        add_action('admin_head', array(
                                                      $this, 'addScripts'
                                                 ));

                        add_action('admin_head', array(
                                                      $this, 'addStyles'
                                                 ));
                    }

                    if (get_option('expiretabs')) {
                        // Check if user wants tabs to expire and schedule
                        add_action('init', array($this, 'schedule'));
                        add_action('expirationdate_expire', array($this, 'runExpire'));
                    } else {
                        // If tabs should not expire unschedule the expiration function
                        wp_unschedule_event(current_time('timestamp'), 'expirationdate_expire');
                    }
                    add_action('admin_menu', array($this, 'modifyMenu'));
                }

            public function addFilters()
                {
                    add_filter('cron_schedules', array($this, 'addCronMinutes'));
                }

            public function addScripts()
                {
                    // Register Scripts
                    wp_register_script('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js', array('jquery'));
                    wp_register_script('jquery-ui-datetime', WP_PLUGIN_URL . '/homepage-slider/js/jquery-ui-datetimepicker.js');
                    wp_register_script('slidercustom', WP_PLUGIN_URL . '/homepage-slider/js/custom.js');
                    wp_register_script('uploadimagebutton', WP_PLUGIN_URL . '/homepage-slider/js/uploadimagebutton.js', array(
                                                                                                                             'jquery',
                                                                                                                             'media-upload',
                                                                                                                             'thickbox'
                                                                                                                        ));

                    // Load Scripts
                    wp_enqueue_script('media-upload');
                    wp_enqueue_script('jquery-color');
                    wp_enqueue_script('jquery-ui');
                    wp_enqueue_script('jquery-ui-datetime');
                    wp_enqueue_script('uploadimagebutton');
                    wp_enqueue_script('slidercustom');
                }

            public function addStyles()
                {
                    // Register Styles
                    wp_register_style('slidertabs', WP_PLUGIN_URL . '/homepage-slider/css/tabs.css');
                    wp_register_style('jquery-ui-custom', WP_PLUGIN_URL . '/homepage-slider/css/custom-theme/jquery-ui-1.8.20.custom.css');

                    // Load Styles
                    wp_enqueue_style('thickbox');
                    wp_enqueue_style('jquery-ui-custom');
                    wp_enqueue_style('slidertabs');
                }

        /**
         * @return bool
         */
            public function isWPMU()
                {
                    if (function_exists('is_multisite')) {
                        return is_multisite();
                    } else {
                        return file_exists(ABSPATH . "/wpmu-settings.php");
                    }
                }

        /**
         * @param array $array
         * @return array
         */
            public function addCronMinutes(array $array)
                {
                    $array['slideexpiratorminute'] = array(
                        'interval' => 60, 'display'  => __('Once a Minute', 'Homepage Slider')
                    );
                    return $array;
                }

        /**
         * @return bool
         */
            public function setTimezone()
                {
                    if (!$timezone_string = get_option('timezone_string')) {
                        return FALSE;
                    }

                    @date_default_timezone_set($timezone_string);
                    return true;
                }

        /**
         * Schedules slide experation functions
         */
            public function schedule()
                {
                    if (!wp_next_scheduled('expirationdate_expire')) {
                        wp_schedule_event(current_time('timestamp'), 'slideexpiratorminute', 'expirationdate_expire');
                    }
                    //left for debugging reasons use this to unschedule the above event for scheduling debugging
                    //wp_unschedule_event(current_time('timestamp'), 'expirationdate_expire');
                }

        /**
         * Expires Tabs
         */
            public function runExpire()
                {
                    //cycle through values and check if its expired
                    for ($i = 1; $i <= get_option('featpost'); $i++) {
                        $expire = get_option('expire' . $i);
                        if (!empty($expire) && strtotime($expire) < current_time('timestamp')) {
                            //set values to variable for easier access
                            $rimgurl = get_option('sImg' . $i . 'replacement');
                            $rimglink = get_option('sImglink' . $i . 'replacement');

                            //replace current slide options with replacement options and set experation to null
                            update_option("sImg$i", $rimgurl);
                            update_option("sImglink$i", $rimglink);
                            update_option('sImg' . $i . 'replacement', '');
                            update_option('sImglink' . $i . 'replacement', '');
                            update_option("expire$i", '');
                        }
                    }
                }

        /**
         * Set default options
         */
            public function install()
                {
                    add_option('sImg1', '', '');
                    add_option('sImg2', '', '');
                    add_option('sImg3', '', '');
                    add_option('sImg4', '', '');
                    add_option('sImg5', '', '');
                    add_option('sImg6', '', '');
                    add_option('sImg7', '', '');
                    add_option('sImg8', '', '');
                    add_option('sImg9', '', '');
                    add_option('sImg10', '', '');
                    add_option('sImglink1', '', '');
                    add_option('sImglink2', '', '');
                    add_option('sImglink3', '', '');
                    add_option('sImglink4', '', '');
                    add_option('sImglink5', '', '');
                    add_option('sImglink6', '', '');
                    add_option('sImglink7', '', '');
                    add_option('sImglink8', '', '');
                    add_option('sImglink9', '', '');
                    add_option('sImglink10', '', '');
                    add_option('source', 'custom');
                    add_option('featcat', '', '');
                    add_option('featpost', '5');
                    add_option('expiretabs', '0');
                }

        /**
         * Remove Default Options for uninstallation
         */
            public static function uninstall()
                {
                    for ($i = 1; $i <= get_option('featpost'); $i++) {
                        //handle image urls
                        delete_option('sImg' . $i);
                        delete_option('sImg' . $i . 'replacement');
                        //handle image links
                        delete_option('sImglink' . $i);
                        delete_option('sImglinkreplacement' . $i);
                        //handle expiration
                        delete_option('expire' . $i);
                    }
                    delete_option('featcat');
                    delete_option('featpost');
                    delete_option('expiretabs');
                }

        /**
         * Update Options in db
         */
            public function save()
                {
                    if ('process' == $_POST['options']) {
                        for ($i = 1; $i <= get_option('featpost'); $i++) {
                            //handle image urls
                            update_option('sImg' . $i, $_REQUEST['sImg' . $i]);
                            update_option('sImg' . $i . 'replacement', $_REQUEST['sImg' . $i . 'replacement']);
                            //handle image links
                            ($_REQUEST['sImglink' . $i] == '') ? update_option('sImglink' . $i, '#') : update_option('sImglink' . $i, $_REQUEST['sImglink' . $i]);
                            ($_REQUEST['sImglink' . $i . 'replacement'] == '') ? update_option('sImglink' . $i . 'replacement', '#') : update_option('sImglink' . $i . 'replacement', $_REQUEST['sImglink' . $i . 'replacement']);
                            //handle image expiration
                            update_option('expire' . $i, $_REQUEST['expire' . $i]);
                        }
                        update_option('source', $_REQUEST['source']);
                        update_option('featcat', $_REQUEST['featcat']);
                        update_option('featpost', $_REQUEST['featpost']);
                        update_option('expiretabs', $_REQUEST['expiretabs']);
                    }

                    // Render Admin Settings Pane
                    $this->renderPanel();

                }

        /**
         * Modify Admin Menu to display homepage slider configuration
         */
            public function modifyMenu()
                {
                    add_menu_page('Homepage Slider', 'Homepage Slider', 'administrator', 'homepage-slider/homeslider.php', array(
                                                                                                                                $this,
                                                                                                                                'save'
                                                                                                                           ), plugin_dir_url(__FILE__) . '/images/slider.png');
                }

        /**
         * Display Admin Backend
         */
            public function renderPanel()
                {
                    ?>
                <div class="wrap">
                    <div id="icon-plugins" class="icon32"></div>
                    <h2>Homepage Slider</h2>

                    <div id="message" class="updated" style="display: none;">
                        <p>
                            <strong>
                                <img src="<?php echo WP_PLUGIN_URL . '/homepage-slider/images/' ?>ajax-loader.gif" alt="loading" />
                                Please Wait
                            </strong>
                        </p>
                    </div>


                    <form method="post" id="slidemanage" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>&updated=true">
                        <input type="hidden" name="options" value="process" />

                        <div id="content-explorer" class="tabs">
                            <ul>
                                <li>
                                    <a href="#tabs-1">Custom Images</a>
                                </li>
                                <li>
                                    <a href="#tabs-2">Usage Settings</a>
                                </li>
                            </ul>
                            <div class="metabox-holdernav" style="">
                                <input class="button-secondary" id="upload_image_button" type="button" value="Upload Image" />
                                <input type="submit" class="button-primary" name="submit" value="Save Changes" style="" />

                                <div style="display:inline;font-style:italic;font-size:11px;padding-left:10px;">
                                    <strong>Important:</strong>
                                    Click 'save changes' after every image you 'insert into post'.
                                </div>
                            </div>

                            <div style="overflow: auto;" id="tabs-1"><!-- first div for content tabs -->
                                <?php if (get_option('source') != 'custom') echo '<div id="message" style="padding:10px;margin:10px 0;border:1px solid #e6db55;background:#ffffe0;"><strong>Custom Images are currently not enabled. To use them, change "Get Images From?" to "Custom Images" under the "Usage Settings" tab.</strong></div>'; ?>
                                <div class="metabox-holder" style="width:402px;float:left;">

                                    <?php

//TODO: ADD MULTIPLE EXPIRATION'S FOR SINGLE SLIDE

                                    for ($i = 1; $i <= get_option('featpost'); $i++) {
                                        ?>
                                        <div class="postbox" id="img_<?php echo $i; ?>">
                                            <!-- FLIP SIDE ONE-->
                                            <div class="side1">
                                                <h3>
                                                    <span>Image #<?php echo $i; ?> link:</span>
                                                    <?php
                                                    if (get_option('expiretabs')) {
                                                        echo '<a href="#" class="alignright expiretoggle">Tab Expires</a>';
                                                    }
                                                    ?>
                                                </h3>

                                                <div class="preview"
                                                    <?php
                                                    if (!get_option('sImg' . $i)) {
                                                        echo 'style="display: none;"';
                                                    }
                                                    ?> >

                                                    <h4 style="margin:10px;">Preview:</h4>
                                                    <img src="<?php echo get_option('sImg' . $i) ?>" style="width:380px; height: 185px; margin:0 10px 10px;" />
                                                </div>

                                                <h4 style="margin:10px;">Image Path:</h4>
                                                <input type="text" <?php if (!get_option('sImg' . $i)) echo 'id="upload_image"'; ?> name="sImg<?php echo $i; ?>" class="imagepath" value="<?php echo stripslashes(get_option('sImg' . $i)); ?>" <?php if (get_option('source') != 'custom') echo 'readonly="readonly"'; ?> style="width: 380px;margin:10px;margin-top:0px;" />
                                                <h4 style="margin:10px;">Image Link:</h4>
                                                <input type="text" name="sImglink<?php echo $i; ?>" class="imagelink" value="<?php echo stripslashes(get_option('sImglink' . $i)); ?>" <?php if (get_option('source') != 'custom') echo 'readonly="readonly"'; ?> style="width: 380px;margin:10px;margin-top:0px;" />
                                            </div>
                                            <!-- FLIP SIDE TWO-->
                                            <div class="side2" style="display: none">
                                                <h3>
                                                    <span>Image #<?php echo $i; ?> replacement:</span>
                                                    <a href="#" class="alignright expiretoggle">Main Slide</a>
                                                </h3>

                                                <h4 style="margin: 10px;">Date/Time to expire</h4>
                                                <input style="margin: 10px;" type="text" class="datetimepicker" id="<?php echo $i; ?>" name="expire<?php echo $i; ?>" value="<?php echo get_option('expire' . $i); ?>" />
                                                <br />

                                                <div class="preview"
                                                    <?php
                                                    if (!get_option('sImg' . $i . 'replacement')) {
                                                        echo 'style="display: none;"';
                                                    }
                                                    ?> >
                                                    <h4 style="margin:10px;">Preview:</h4>
                                                    <img src="<?php echo get_option('sImg' . $i . 'replacement') ?>" style="width:380px; height: 185px; margin:0 10px 10px;" />
                                                </div>

                                                <h4 style="margin:10px;">Image Path:</h4>
                                                <input type="text" <?php if (!get_option('sImg' . $i . 'replacement')) echo 'id="upload_image"'; ?> name="sImg<?php echo $i; ?>replacement" class="imagepath" value="<?php echo stripslashes(get_option('sImg' . $i . 'replacement')); ?>" <?php if (get_option('source') != 'custom') echo 'readonly="readonly"'; ?> style="width: 380px;margin:10px;margin-top:0px;" />
                                                <h4 style="margin:10px;">Image Link:</h4>
                                                <input type="text" name="sImglink<?php echo $i; ?>replacement" class="imagelink" value="<?php echo stripslashes(get_option('sImglink' . $i . 'replacement')); ?>" <?php if (get_option('source') != 'custom') echo 'readonly="readonly"'; ?> style="width: 380px;margin:10px;margin-top:0px;" />

                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div id="tabs-2"><!--options tab -->
                                <div class="metabox-holder" style="width:815px;">
                                    <div class="postbox">
                                        <table class="form-table" style="margin:0;">
                                            <tr valign="top">
                                                <td style="padding:0;width:180px;">
                                                    <h3>Name</h3>
                                                </td>
                                                <td style="padding:0;width:235px;">
                                                    <h3>Value</h3>
                                                </td>
                                                <td style="padding:0;">
                                                    <h3>Description</h3>
                                                </td>
                                            </tr>

                                            <tr valign="top" style="border-bottom:1px solid #ccc;">
                                                <td style="padding:5px 0;">
                                                    <label for="source" style="padding:10px;font-weight:bold;">Get
                                                                                                               Images
                                                                                                               From?</label>
                                                </td>
                                                <td style="padding:5px 0;">
                                                    <select name="source" style="width:235px;">
                                                        <option value="custom" <?php selected('custom', get_option('source')); ?>>
                                                            Custom
                                                            Images
                                                        </option>
                                                    </select>
                                                </td>
                                                <td style="padding:5px 0;">
                                                    <p style="margin:0 10px;font-style:italic;font-size:11px;">
                                                        Here you can select the source from which the images are
                                                        displayed. </p>
                                                </td>
                                            </tr>

                                            <tr valign="top" style="border-bottom:1px solid #ccc;">
                                                <td style="padding:5px 0;">
                                                    <label for="featpost" style="padding:10px;font-weight:bold;">Number
                                                                                                                 of
                                                                                                                 Slides:</label>
                                                </td>
                                                <td style="padding:5px 0;"
                                                ">
                                                <input type="text" id="test" style="width:50px;" name="featpost" value="<?php echo get_option('featpost'); ?>" />
                                                </td>
                                                <td style="padding:5px 0;">
                                                    <p style="margin:0 10px;font-style:italic;font-size:11px;">
                                                        Number of post thumbnails to be displayed in the slider. No
                                                        matter how high
                                                        you
                                                        set
                                                        it, the slider will display a maximum of 10 images. </p>
                                            </tr>


                                            <tr valign="top" style="border-bottom:1px solid #ccc;">
                                                <td style="padding:5px 0;">
                                                    <label for="expire" style="padding:10px;font-weight:bold;">Expire
                                                                                                               Tabs</label>
                                                </td>
                                                <td style="padding:5px 0;">
                                                    <input type="checkbox" name="expiretabs" <?php if (get_option('expiretabs')) echo "checked='checked'"; ?>/>
                                                    Enable
                                                </td>
                                                <td style="margin:5px 0;">
                                                    <p style="margin:0;font-style:italic;font-size:11px;">
                                                        Check this box to enable the ability to have tabs expire based
                                                        on date.
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- #content-explorer -->
                    </form>
                </div>
                <?php
                }

        /**
         * Display Slider to the public
         */
            public static function renderSlider()
                {
                    for ($i = 1; $i <= get_option('featpost'); $i++) {
                        if (get_option("sImg$i")) {
                            ?>
                        <a href="<?php echo get_option("sImglink$i"); ?> '">
                            <img src="<?php echo get_option("sImg$i"); ?>" alt="" />
                        </a>
                        <?php
                        }
                    }
                }
        }

    new HomepageSlider();

    function homeSlider()
        {
            echo HomepageSlider::renderSlider();
        }