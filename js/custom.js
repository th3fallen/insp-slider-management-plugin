/*
*    Document   : custom
*   Created on : May 21, 2012, 11:28:04 AM
*    Author     : ClarkT AKA clarktomlinson
*    Web        : http://www.clarkt.com
*    Copyright  : 2012
*/

//async submit slide management form
function submitSlideManager(){
    var form = jQuery('#slidemanage');
    $ = jQuery;

    $.ajax({
        type: 'POST',
        url: form.attr('action'),
        data: form.serialize(),
        beforeSend: function() {
            $('.updated').fadeIn('fast');
        },
        success: function (data) {
            $('.updated').html('<p><strong>Slider Options Updated.</strong></p>').fadeIn('fast');
        },
        error: function(data) {
            $('.updated').html('<p><strong>Error Updating Slider Options <a href="#" class="button-primary" onclick=submitSlideManager()"> Try again</a></strong></p>').fadeIn('fast');
        }
    });
};

jQuery(document).ready(function($) {
    //init $ui tabs
    $( "#content-explorer" ).tabs();

    // init box flipping
    $('.metabox-holder').on('click', '.expiretoggle', function() {
        $side2 = $(this);
        $side2.parentsUntil('.metabox-holder', '.postbox').find('.side1').slideToggle('slow', function () {
            $side2.parentsUntil('.metabox-holder', '.postbox').find('.side2').slideToggle('slow');
        });
    });

    //init sortable for slide reordering
    $(".metabox-holder").sortable({
        connectWith: '.metabox-holder',
        update: function() {
            var i = 1;
            $('.postbox').each(function() {
                $(this).find('h3 span').html('Image #'+i+' Link:');
                $(this).find('.imagepath').attr('name', 'sImg'+i);
                $(this).find('.imagelink').attr('name', 'sImglink'+i);
                $(this).find('#date').attr('name', 'expire'+i);
                $(this).find('.side2 .imagepath').attr('name', 'sImg'+i+'replacement');
                $(this).find('.side2 .imagelink').attr('name', 'sImglink'+i+'replacement');
                i++;
            });
            submitSlideManager();
        }
    });

    //init async image previewing/submission
    $('.metabox-holder').on('change', '.imagepath', function() {
        //set current selector to varable for reuse
        var $el = $(this);
        //get field value and assign to vararble
        var $imgurl = $el.val();
        //check updated value and see if its valid to submit
        if ($imgurl) {
            $el.parent().find('.preview').fadeOut('fast', function() {
                $el.parent().find('img').attr('src', $imgurl);
                $el.parent().find('.preview').fadeIn('fast');
            });
        //            $el.parent().find('.preview').fadeIn('fast').find('img').attr('src', imgurl);
        } else {
            $el.parent().find('.preview').fadeOut('fast');
        }
        submitSlideManager();
        $el.effect('highlight', {
            color: '#DFF0B8'
        }, 'slow');
    });

    //init async link submission
    $('.metabox-holder').on('change', '.imagelink', function() {
        submitSlideManager();
        $(this).effect('highlight', {
            color: '#DFF0B8'
        }, 'slow');
    });

    //init async datepicker submission
    $('.metabox-holder').on('change', '.datetimepicker', function() {
        submitSlideManager();
        $(this).effect('highlight', {
            color: '#DFF0B8'
        }, 'slow');
    });

    $('.metabox-holder').on('focus', '.datetimepicker', function() {
        var id = $(this).attr('id');
        $('#' + id).datetimepicker({
            ampm: true
        });
    });


});